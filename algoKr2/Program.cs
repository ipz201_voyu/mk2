﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace algoKr2
{
    struct Uzel
    {
        public string symbol { get; set; }
        public int count { get; set; }
        public string binCode { get; set; }
        public Uzel(string symbol, int count, string binCode)
        {
            this.symbol = symbol;
            this.count = count;
            this.binCode = binCode;
        }
    };

    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            List<Uzel> newRes = new List<Uzel>();
            List<Uzel> tree = new List<Uzel>();
            Dictionary<char, int> countSymbols = new Dictionary<char, int>();
            string str = Console.ReadLine();
            for (int i = 0; i < str.Length; i++)
            {
                if (countSymbols.Keys.Contains(str[i]) == false)
                {
                    countSymbols.Add(str[i], 1);
                }
                else
                {
                    countSymbols[str[i]]++;
                }
            }
            foreach (var countSymbol in countSymbols)
            {
                newRes.Add(new Uzel(countSymbol.Key.ToString(), countSymbol.Value, ""));
                tree.Add(new Uzel(countSymbol.Key.ToString(), countSymbol.Value, ""));
            }


            while (tree.Count > 1)
            {
                tree.Sort((Uzel a, Uzel b) =>
                {
                    if (a.count > b.count)
                    {
                        return 1;
                    }
                    else
                    {
                        return -1;
                    }
                });
                int i = 0;
                foreach (var countSymbol in countSymbols) {
                    if (tree[0].symbol.Contains(countSymbol.Key))
                    {
                        newRes[i] = new Uzel(newRes[i].symbol, newRes[i].count, "1" + newRes[i].binCode);
                    }
                    else if (tree[1].symbol.Contains(countSymbol.Key))
                    {
                        newRes[i] = new Uzel(newRes[i].symbol, newRes[i].count, "0" + newRes[i].binCode);
                    }
                    i++;
                }
                tree[1] = new Uzel(tree[0].symbol + tree[1].symbol, tree[0].count + tree[1].count, "");
                tree.RemoveAt(0);
            }
            Console.WriteLine();
            Console.WriteLine($"Алфавіт програми: ");
            for (int i = 0; i < countSymbols.Count; i++)
            {
                Console.WriteLine(newRes[i].symbol + " (" + newRes[i].binCode + ")");
            }
            Console.WriteLine();
            string dopStr = "", dopStr1="", d="";
            for(int i = 0; i < str.Length; i++)
            {
                for (int j = 0; j < countSymbols.Count; j++)
                {
                    if (str[i].ToString() == newRes[j].symbol)
                    {
                        dopStr += newRes[j].binCode;
                    }
                }
            }
            Console.WriteLine($"Перетворений рядок:\n{dopStr}");
            for (int i = 0; i < dopStr.Length; i++)
            {
                d += dopStr[i];
                int k = 0;
                for (int j = 0; j < countSymbols.Count; j++)
                {
                    if (d == newRes[j].binCode)
                    {
                        dopStr1 += newRes[j].symbol;
                        k = 1;
                    }
                }
                if (k == 1)
                {
                    d = "";
                }
            }
            Console.WriteLine($"Перетворений рядок у звичайну форму, щоб показати, що стиснення виконується коректно:\n{dopStr1}");
            Console.WriteLine("Коефіціент стиснення: {0}", 8.0*str.Length/dopStr.Length);
        }
    }
}
